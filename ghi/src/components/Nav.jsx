import { NavLink } from 'react-router-dom'

const Nav = () => {
    return (
        <nav>
            <div>
                <NavLink to="/">
                    Home
                </NavLink>
                <NavLink to="/dashboard">
                    Dashboard
                </NavLink>
                <NavLink to="/profile">
                    Profile
                </NavLink>
            </div>
        </nav>
    )
}

export default Nav
