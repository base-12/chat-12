import '/src/styles/App.css'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Nav from '/src/components/Nav'
import Landing from '/src/components/Landing'
import Dashboard from './components/Dashboard'
import Profile from './components/Profile'

function App() {
  return (
    <Router>
      <Nav />
      <div>
        <Routes>
          <Route path="/" element={<Landing />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/profile" element={<Profile />} />
        </Routes>
      </div>
    </Router>
  )
}

export default App
