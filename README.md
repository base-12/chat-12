<!--
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/base-12/chat-12.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/base-12/chat-12/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
-->

# Chat 12 <img src="./docs/base-12.png" width="40px" align="right" style="object-fit: cover; margin-right: 2rem; border-radius: 10%" />
`A Base 12 Product`

## An instant-messaging app for the professional dev
Connect with colleagues and freinds at any time. Organize your conversations however you like, and find them at a glance on our dashboard. Work and relate with equal ease using the chat-style or rich-text style editors.

## Index

- [The Base 12 Team](#the-base-12-team)
- [Running the Dev Environment](#running-the-dev-environment)
- [Technical Description](#technical-description)
  - [Data Structure](#data-structure)

## The Base 12 Team
- Rachel Allen
- Denny Bucklin
- Hunter Lang
- Xetta Snyder
- Brian Stormes

## Running the Dev Environment
[Top](#index)

To run the dev environment, follow these steps:

(Make sure you have Docker, and Git installed)

1. Visit our Gitlab: `https://gitlab.com/base-12/chat-12`

2. Fork and clone the repository onto your local computer.

3. Create a file named `.env` in the root directory and add the following lines:
```md
CORS_HOST="*"  # use * to access the index through the file structure or localhost to access through a network
MONGO_HOST="mongodb://root:example@127.0.0.1:27017/admin"
```

4. Build and run the project using Docker with these terminal commands:

```
    docker volume create chat-db
    docker-compose build
    docker-compose up
```

5. Connect to MongoDB using the following terminal command:

```
    docker exec -it chat-12-db-1 mongosh mongodb://root:example@127.0.0.1:27017/admin
```
- After running these commands, make sure all of your Docker containers are running
- View the project in the browser by navigating to `index.html`

## Technical Description
### Data Structure
MongoDB databases

**Users**
```
    role: List[string]
    screen_name: string
    password: hashed string
    email: string
    chats: List[Chats]
    _id: int PK
```

**Images**
```
    content: bytes
    alt: string
    _id: int PK
```

**Chats**
```
    root: Message._id
    tail: Message._id
    position: Tuple[x: int, y: int, width: int, height: int]
    created: datetime
    _id: int PK
```

**Messages**
```
    parent: Message._id
    child: Message._id
    title: string
    content: string
    rich_text: boolean
    created: datetime
    _id: int PK
```

<!-- TODO -->
## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

