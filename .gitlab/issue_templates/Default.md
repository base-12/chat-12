## Chat 12 by Base 12

### Task:

Story: As a [user] i can [do something].

OR

Problem: When [event] occurs, [something happens].

### Acceptance Criteria:

- [ ] As a new [user], I can [enter my email and password into a form on the frontend]
- [ ] This sends a [request to the backend which creates a row in the DB]
- [ ] [A 200 response is sent to the frontend and the user is redirected to their dashboard (a page only visible when logged in)]

### Definition of Done:

- [ ] Documentation (if needed)
- [ ] Source code
- [ ] Unit test(s) (if needed)
- [ ] Passing CI/CD pipeline(s)

### Notes:

This section can include articles and other resources that might help the developer.
