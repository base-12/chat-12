import pytest
from main import create_app
from unittest.mock import MagicMock


@pytest.fixture()
def app():
    app = create_app()
    app.config["db"] = MagicMock()
    app.config["db"].images = MagicMock()
    app.config["db"].messages = MagicMock()
    app.config["db"].users = MagicMock()
    app.config["db"].chats = MagicMock()
    app.config["TESTING"] = True
    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()
