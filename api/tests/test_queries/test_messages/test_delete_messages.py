from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock, patch
from flask import Flask
from queries.messages import MessageRepo


def find_one(query: dict):
    if query.get("_id") == ObjectId('111000000000000000000000'):
        return {
            "_id": ObjectId('111000000000000000000000'),
            "title": "Title 1",
            "content": "Content 1",
            "parent_message_id": None,
            "child_message_id": "222000000000000000000000",
            "rich_text": False,
            "created": "2022-01-01T00:00:00"
        }
    elif query.get("_id") == ObjectId("222000000000000000000000"):
        return {
            "_id": ObjectId('222000000000000000000000'),
            "title": "Title 2",
            "content": "Content 2",
            "parent_message_id": '111000000000000000000000',
            "child_message_id": '333000000000000000000000',
            "rich_text": False,
            "created": "2022-01-01T01:00:00"
        }
    elif query.get("_id") == ObjectId('333000000000000000000000'):
        return {
            "_id": ObjectId('333000000000000000000000'),
            "title": "Title 3",
            "content": "Content 3",
            "parent_message_id": "222000000000000000000000",
            "child_message_id": None,
            "rich_text": False,
            "created": "2022-01-01T02:00:00"
        }
    else:
        return None


class TestDeleteMessage(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self) -> None:
        self.ctx.pop()

    def test_delete_message(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one.return_value = {
            "_id": ObjectId('111000000000000000000000'),
            "title": "Title 1",
            "content": "Content 1",
            "parent_message_id": None,
            "child_message_id": None,
            "rich_text": False,
            "created": "2022-01-01T00:00:00"
        }
        self.app.config["db"].messages = mock_messages
        mock_messages.delete_one.return_value.deleted_count = 1
        repo = MessageRepo()

        # Act
        result = repo.delete_message("111000000000000000000000")

        # Assert
        self.assertEqual(result, True)
        mock_messages.delete_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')}
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_delete_id_not_found(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one.return_value = None
        self.app.config["db"].messages = mock_messages
        repo = MessageRepo()

        # Act
        result = repo.delete_message("111000000000000000000000")

        # Assert
        self.assertEqual(result, False)
        mock_messages.delete_one.assert_not_called()

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_delete_parent_of_child(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one = find_one
        mock_messages.delete_one.return_value.deleted_count = 1
        self.app.config["db"].messages = mock_messages
        mock_update = MagicMock()
        repo = MessageRepo()

        # Act
        with patch.object(MessageRepo, "update_message", new=mock_update):
            result = repo.delete_message("111000000000000000000000")

        # Assert
        self.assertEqual(result, True)
        mock_update.assert_called_once_with(
            "222000000000000000000000",
            parent_message_id=None
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_delete_child_of_parent(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one = find_one
        mock_messages.delete_one.return_value.deleted_count = 1
        self.app.config["db"].messages = mock_messages
        mock_update = MagicMock()
        repo = MessageRepo()

        # Act
        with patch.object(MessageRepo, "update_message", new=mock_update):
            result = repo.delete_message("333000000000000000000000")

        # Assert
        self.assertEqual(result, True)
        mock_update.assert_called_once_with(
            "222000000000000000000000",
            child_message_id=None
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_delete_with_child_and_parent(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one = find_one
        mock_messages.delete_one.return_value.deleted_count = 1
        self.app.config["db"].messages = mock_messages
        mock_update = MagicMock()
        repo = MessageRepo()

        # Act
        with patch.object(MessageRepo, "update_message", new=mock_update):
            result = repo.delete_message("222000000000000000000000")

        # Assert
        self.assertEqual(result, True)
        mock_update.assert_any_call(
            "111000000000000000000000",
            child_message_id="333000000000000000000000"
        )
        mock_update.assert_any_call(
            "333000000000000000000000",
            parent_message_id="111000000000000000000000"
        )

        # Clean Up
        self.app.config["db"] = MagicMock()
