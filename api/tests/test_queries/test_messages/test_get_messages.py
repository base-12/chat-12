from datetime import datetime
from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from models.pydantic import DatabaseError, MessageOut
from queries.messages import MessageRepo


def find(query: dict):
    if (query.get("_id") == ObjectId('111000000000000000000000') or
       query.get("title") == "Title 1"):
        return [
            {
                "_id": ObjectId('111000000000000000000000'),
                "title": "Title 1",
                "content": "Content 1",
                "parent_message_id": None,
                "child_message_id": None,
                "rich_text": False,
                "created": datetime(2022, 1, 1, 0, 0, 0)
            }
        ]
    elif query.get("_id") == ObjectId("222000000000000000000000"):
        return [
            {
                "_id": ObjectId('222000000000000000000000'),
                "title": "Title 2",
                "content": "Content 2",
                "parent_message_id": None,
                "child_message_id": '333000000000000000000000',
                "rich_text": False,
                "created": datetime(2022, 1, 1, 1, 0, 0)
            }
        ]
    elif query.get("_id") == ObjectId('333000000000000000000000'):
        return [
            {
                "_id": ObjectId('333000000000000000000000'),
                "title": "Title 3",
                "content": "Content 3",
                "parent_message_id": "222000000000000000000000",
                "child_message_id": None,
                "rich_text": False,
                "created": datetime(2022, 1, 1, 2, 0, 0)
            }
        ]
    elif query == {}:
        return [
            {
                "_id": ObjectId('111000000000000000000000'),
                "title": "Title 1",
                "content": "Content 1",
                "parent_message_id": None,
                "child_message_id": None,
                "rich_text": False,
                "created": datetime(2022, 1, 1, 0, 0, 0)
            },
            {
                "_id": ObjectId('222000000000000000000000'),
                "title": "Title 2",
                "content": "Content 2",
                "parent_message_id": None,
                "child_message_id": '333000000000000000000000',
                "rich_text": False,
                "created": datetime(2022, 1, 1, 1, 0, 0)
            },
            {
                "_id": ObjectId('333000000000000000000000'),
                "title": "Title 3",
                "content": "Content 3",
                "parent_message_id": "222000000000000000000000",
                "child_message_id": None,
                "rich_text": False,
                "created": datetime(2022, 1, 1, 2, 0, 0)
            }
        ]
    # nothing found
    else:
        return []


class TestInsertMessages(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_get_messages(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find = find
        self.app.config["db"].messages = mock_collection

        # testing source and expected result
        message_repo = MessageRepo()

        # Act
        result = message_repo.get_messages()

        # Assert
        self.assertEqual(len(result), 3)
        self.assertIsInstance(result[0], MessageOut)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_messages_by_id(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find = find
        self.app.config["db"].messages = mock_collection

        # testing source and expected result
        message_repo = MessageRepo()
        expected_result = [
            MessageOut(
                id="222000000000000000000000",
                title="Title 2",
                content="Content 2",
                parent_message_id=None,
                child_message_id='333000000000000000000000',
                rich_text=False,
                created="2022-01-01T01:00:00",
            )
        ]

        # Act
        result = message_repo.get_messages(
            id="222000000000000000000000"
        )

        # Assert
        self.assertEqual(result, expected_result)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_key_not_in_model(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find = find

        # testing source and expected result
        message_repo = MessageRepo()

        # Act
        result = message_repo.get_messages(
            error="anything"
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_nothing_found(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find = find
        self.app.config["db"].messages = mock_collection

        # testing source and expected result
        message_repo = MessageRepo()

        # Act
        result = message_repo.get_messages(
            id="444000000000000000000000"
        )

        # Assert
        self.assertEqual(result, [])

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_by_title(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find = find
        self.app.config["db"].messages = mock_collection

        # testing source and expected result
        message_repo = MessageRepo()
        expected_result = [
            MessageOut(
                id="111000000000000000000000",
                title="Title 1",
                content="Content 1",
                parent_message_id=None,
                child_message_id=None,
                rich_text=False,
                created="2022-01-01T00:00:00",
            )
        ]

        # Act
        result = message_repo.get_messages(
            title="Title 1"
        )

        # Assert
        self.assertEqual(result, expected_result)

        # Clean Up
        self.app.config["db"] = MagicMock()
