from datetime import datetime
from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock, patch
from models.pydantic import DatabaseError, MessageOut
from queries.messages import MessageRepo
from flask import Flask


def update_one(query: dict, update: dict):
    result = MagicMock()
    result.modified_count = 1 if (
        query.get("_id") == ObjectId('111000000000000000000000')
    ) else 0
    return result


class TestUpdateMessage(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_update_message(self):
        # Arrange
        fake_message = MessageOut(
            id="111000000000000000000000",
            title="Title 1",
            content="Content 1",
            parent_message_id=None,
            child_message_id=None,
            rich_text=False,
            created=datetime(2022, 1, 1, 0, 0, 0)
        )
        mock_messages = MagicMock()
        mock_messages.update_one = update_one
        self.app.config["db"].messages = mock_messages
        repo = MessageRepo()
        mock_get_messages = MagicMock()
        mock_get_messages.return_value = [
            fake_message
        ]

        # Act
        with patch.object(MessageRepo, "get_messages", new=mock_get_messages):
            result = repo.update_message(
                "111000000000000000000000",
                title="New Title"
            )

        # Assert
        self.assertEqual(result, fake_message)
        mock_get_messages.assert_called_once_with(
            id="111000000000000000000000"
        )

        # Clean Up
        self.app.config["db"].messages = MagicMock()

    def test_update_key_not_in_model(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.update_one = update_one
        self.app.config["db"].messages = mock_messages
        repo = MessageRepo()

        # Act
        result = repo.update_message(
            "111000000000000000000000",
            subtitle="New Title"
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"].messages = MagicMock()

    def test_update_id_not_found(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.update_one = update_one
        repo = MessageRepo()

        # Act
        result = repo.update_message(
            "111000000000000000011100",
            title="New Title"
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"].messages = MagicMock()
