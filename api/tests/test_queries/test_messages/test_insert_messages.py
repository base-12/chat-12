from datetime import datetime
from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock, patch

from flask import Flask
from models.pydantic import DatabaseError, MessageIn, MessageOut
from queries.messages import MessageRepo


def find_one(query: str):
    # parent without child
    if query.get("_id") == ObjectId('111000000000000000000000'):
        return {
            "_id": query.get("_id"),
            "child_message_id": None
        }
    # parent with child
    elif query.get("_id") == ObjectId("222000000000000000000000"):
        return {
            "_id": query.get("_id"),
            "child_message_id": "333000000000000000000000"
        }
    # child of parent_with_child
    elif query.get("_id") == ObjectId('333000000000000000000000'):
        return {
            "_id": query.get("_id"),
            "parent_message_id": "222000000000000000000000"
        }
    # searching by child_message_id
    elif query.get("child_message_id") == "333000000000000000000000":
        return {
            "_id": ObjectId("222000000000000000000000"),
            "child_message_id": "333000000000000000000000"
        }
    # nothing found
    else:
        return None


class TestInsertMessages(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_insert_message(self):
        # Arrange
        fake_message = MessageIn(
            title="Test Title",
            content="Test Content",
            rich_text=False,
            created="2022-01-01T00:00:00",
        )
        mock_collection = MagicMock()
        mock_collection.find_one = find_one
        mock_result = MagicMock()
        mock_result.inserted_id = "inserted_id"
        mock_collection.insert_one.return_value = mock_result
        self.app.config["db"].messages = mock_collection

        #  testing source and expected result
        message_repo = MessageRepo()
        expected_result = MessageOut(
            id="inserted_id",
            title="Test Title",
            content="Test Content",
            parent_message_id=None,
            child_message_id=None,
            rich_text=False,
            created="2022-01-01T00:00:00",
        )

        # Act
        result = message_repo.insert_message(fake_message)

        # Assert
        self.assertEqual(result, expected_result)
        mock_collection.insert_one.assert_called_once_with(
            {
                "title": "Test Title",
                "content": "Test Content",
                "parent_message_id": None,
                "child_message_id": None,
                "rich_text": False,
                "created": datetime(2022, 1, 1, 0, 0),
            }
        )

        # Clean up
        self.app.config["db"] = MagicMock()

    def test_insert_message_with_parent(self):
        # Arrange
        fake_message = MessageIn(
            title="Test Title",
            content="Test Content",
            parent_message_id='111000000000000000000000',
            rich_text=True,
            created="2022-01-01T00:00:00",
        )
        mock_collection = MagicMock()
        mock_collection.find_one = find_one
        mock_result = MagicMock()
        mock_result.inserted_id = "inserted_id"
        mock_collection.insert_one.return_value = mock_result
        self.app.config["db"].messages = mock_collection
        mock_update_message = MagicMock()

        #  testing source and expected result
        message_repo = MessageRepo()
        expected_result = MessageOut(
            id="inserted_id",
            title="Test Title",
            content="Test Content",
            parent_message_id="111000000000000000000000",
            child_message_id=None,
            rich_text=True,
            created="2022-01-01T00:00:00",
        )

        # Act
        with patch.object(
            MessageRepo,
            'update_message',
            new=mock_update_message
        ):
            result = message_repo.insert_message(fake_message)

        # Assert
        self.assertEqual(result, expected_result)
        mock_update_message.assert_called_once_with(
            id='111000000000000000000000',
            child_message_id='inserted_id'
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_insert_message_with_child(self):
        # Arrange
        fake_message = MessageIn(
            title="Test Title",
            content="Test Content",
            child_message_id='111000000000000000000000',
            rich_text=True,
            created="2022-01-01T00:00:00",
        )
        mock_collection = MagicMock()
        mock_collection.find_one = find_one
        mock_result = MagicMock()
        mock_result.inserted_id = "inserted_id"
        mock_collection.insert_one.return_value = mock_result
        self.app.config["db"].messages = mock_collection
        mock_update_message = MagicMock()

        #  testing source and expected result
        message_repo = MessageRepo()
        expected_result = MessageOut(
            id="inserted_id",
            title="Test Title",
            content="Test Content",
            child_message_id="111000000000000000000000",
            parent_message_id=None,
            rich_text=True,
            created="2022-01-01T00:00:00",
        )

        # Act
        with patch.object(
            MessageRepo,
            'update_message',
            new=mock_update_message
        ):
            result = message_repo.insert_message(fake_message)

        # Assert
        self.assertEqual(result, expected_result)
        mock_update_message.assert_called_once_with(
            id="111000000000000000000000",
            parent_message_id="inserted_id"
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_insert_new_parent_fails(self):
        # Arrange
        fake_message = MessageIn(
            title="Test Title",
            content="Test Content",
            parent_message_id='222000000000000000000000',
            rich_text=True,
            created="2022-01-01T00:00:00",
        )
        mock_collection = MagicMock()
        mock_collection.find_one = find_one
        mock_result = MagicMock()
        mock_result.inserted_id = "inserted_id"
        mock_collection.insert_one.return_value = mock_result
        self.app.config["db"].messages = mock_collection

        #  testing source and expected result
        message_repo = MessageRepo()

        # Act
        result = message_repo.insert_message(fake_message)

        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_insert_new_child_fails(self):
        # Arrange
        fake_message = MessageIn(
            title="Test Title",
            content="Test Content",
            child_message_id='333000000000000000000000',
            rich_text=True,
            created="2022-01-01T00:00:00",
        )
        mock_collection = MagicMock()
        mock_collection.find_one = find_one
        mock_result = MagicMock()
        mock_result.inserted_id = "inserted_id"
        mock_collection.insert_one.return_value = mock_result
        self.app.config["db"].messages = mock_collection

        #  testing source and expected result
        message_repo = MessageRepo()

        # Act
        result = message_repo.insert_message(fake_message)

        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"] = MagicMock()
