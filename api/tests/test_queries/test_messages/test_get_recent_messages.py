from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from models.pydantic import MessageOut
from queries.messages import MessageRepo
from flask import Flask


def find_one(query: str):
    # parent without child
    if query.get("_id") == ObjectId('111000000000000000000000'):
        return {
            "_id": ObjectId('111000000000000000000000'),
            "title": "Title 1",
            "content": "Content 1",
            "parent_message_id": None,
            "child_message_id": "222000000000000000000000",
            "rich_text": False,
            "created": "2022-01-01T00:00:00"
        }
    # parent with child
    elif query.get("_id") == ObjectId("222000000000000000000000"):
        return {
            "_id": ObjectId("222000000000000000000000"),
            "title": "Title 2",
            "content": "Content 2",
            "parent_message_id": '111000000000000000000000',
            "child_message_id": "333000000000000000000000",
            "rich_text": False,
            "created": "2022-01-01T01:00:00"
        }
    # child of parent_with_child
    elif query.get("_id") == ObjectId('333000000000000000000000'):
        return {
            "_id": ObjectId('333000000000000000000000'),
            "title": "Title 3",
            "content": "Content 3",
            "parent_message_id": "222000000000000000000000",
            "child_message_id": None,
            "rich_text": False,
            "created": "2022-01-01T02:00:00"
        }
    # nothing found
    else:
        return None


class TestGetRecent(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_get_recent_from_orphan(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one = find_one
        self.app.config["db"].messages = mock_messages
        repo = MessageRepo()

        # Act
        result = repo.get_recent_messages("111000000000000000000000")

        # Assert
        self.assertEqual(len(result), 1)
        self.assertIsInstance(result[0], MessageOut)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_2_recent(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one = find_one
        self.app.config["db"].messages = mock_messages
        repo = MessageRepo()

        # Act
        result = repo.get_recent_messages("333000000000000000000000", 2)

        # Assert
        self.assertEqual(len(result), 2)
        self.assertIsInstance(result[0], MessageOut)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_recent(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one = find_one
        self.app.config["db"].messages = mock_messages
        repo = MessageRepo()

        # Act
        result = repo.get_recent_messages("333000000000000000000000")

        # Assert
        self.assertEqual(len(result), 3)
        self.assertIsInstance(result[0], MessageOut)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_recent_id_not_found(self):
        # Arrange
        mock_messages = MagicMock()
        mock_messages.find_one.return_value = None
        self.app.config["db"].messages = mock_messages
        repo = MessageRepo()

        # Act
        result = repo.get_recent_messages("000000000000000000000000")

        # Assert
        self.assertEqual(len(result), 0)

        # Clean Up
        self.app.config["db"] = MagicMock()
