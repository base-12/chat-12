from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from models.pydantic import DatabaseError, ImageIn, ImageOut
from queries.images import ImageRepo
from PIL import Image
import io


class TestInsertImages(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()
        img = Image.new('RGB', (1, 1))
        img_byte_arr = io.BytesIO()
        img.save(img_byte_arr, format='PNG')
        self.dummy_image = img_byte_arr.getvalue()

    def tearDown(self):
        self.ctx.pop()

    def test_insert_image(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.insert_one.return_value.inserted_id = ObjectId(
            '111000000000000000000000'
        )
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.insert_image(
            ImageIn(
                alt="Title 1",
                content=self.dummy_image
            )
        )

        # Assert
        self.assertIsInstance(result, ImageOut)
        self.assertEqual(result.content, self.dummy_image)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_insert_db_failure(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.insert_one.return_value.inserted_id = None
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.insert_image(
            ImageIn(
                alt="Title 1",
                content=self.dummy_image
            )
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_insert_image_database_error(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.insert_one.side_effect = Exception("Boom")
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.insert_image(
            ImageIn(
                alt="Title 1",
                content=self.dummy_image
            )
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"] = MagicMock()
