from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock, patch
from flask import Flask
from models.pydantic import DatabaseError, ImageOut
from queries.images import ImageRepo
from PIL import Image
import io


class TestUpdateImages(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()
        img = Image.new('RGB', (1, 1))
        img_byte_arr = io.BytesIO()
        img.save(img_byte_arr, format='PNG')
        self.dummy_image = img_byte_arr.getvalue()

    def tearDown(self):
        self.ctx.pop()

    def test_update_image(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.update_one.return_value.modified_count = 1
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()
        mock_get_images = MagicMock()
        mock_get_images.return_value = [
            ImageOut(
                id='111000000000000000000000',
                alt="Title 1",
                content=self.dummy_image
            )
        ]

        # Act
        with patch.object(ImageRepo, "get_images", new=mock_get_images):
            result = image_repo.update_image(
                "111000000000000000000000",
                alt="Title 1",
                content=self.dummy_image
            )

        # Assert
        self.assertEqual(result, ImageOut(
            id='111000000000000000000000',
            alt="Title 1",
            content=self.dummy_image
        ))
        mock_collection.update_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')},
            {"$set": {
                "alt": "Title 1",
                "content": self.dummy_image
            }}
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_update_image_not_found(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.update_one.return_value.modified_count = 0
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()
        mock_get_images = MagicMock()

        # Act
        with patch.object(ImageRepo, "get_images", new=mock_get_images):
            result = image_repo.update_image(
                "111000000000000000000000",
                alt="Title 1",
                content=self.dummy_image
            )

        # Assert
        self.assertIsInstance(result, DatabaseError)
        mock_get_images.assert_not_called()

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_update_database_error(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.update_one.side_effect = Exception("Boom")
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()
        mock_get_images = MagicMock()

        # Act
        with patch.object(ImageRepo, "get_images", new=mock_get_images):
            result = image_repo.update_image(
                "111000000000000000000000",
                alt="Title 1",
                content=self.dummy_image
            )

        # Assert
        self.assertIsInstance(result, DatabaseError)
        mock_get_images.assert_not_called()

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_update_key_not_in_model(self):
        # Arrange
        mock_collection = MagicMock()
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.update_image(
            "111000000000000000000000",
            error="anything"
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"] = MagicMock()
