from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from models.pydantic import DatabaseError, ImageOut
from queries.images import ImageRepo
from PIL import Image
import io


class TestGetImages(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()
        img = Image.new('RGB', (1, 1))
        img_byte_arr = io.BytesIO()
        img.save(img_byte_arr, format='PNG')
        self.dummy_image = img_byte_arr.getvalue()

    def tearDown(self):
        self.ctx.pop()

    def test_get_images(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find.return_value = [
            {
                "_id": ObjectId('111000000000000000000000'),
                "alt": "Title 1",
                "content": self.dummy_image
            }
        ]
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.get_images(id='111000000000000000000000')
        # Assert
        self.assertEqual(result, [
            ImageOut(
                id='111000000000000000000000',
                alt="Title 1",
                content=self.dummy_image
            )
        ])

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_images_not_found(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find.return_value = []
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.get_images(id='111000000000000000000000')
        # Assert
        self.assertEqual(len(result), 0)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_get_images_database_err(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find.side_effect = Exception("Boom")
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.get_images(id='111000000000000000000000')
        # Assert
        self.assertIsInstance(result, DatabaseError)

        # Clean Up
        self.app.config["db"] = MagicMock()
