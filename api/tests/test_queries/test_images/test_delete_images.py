from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from models.pydantic import DatabaseError
from queries.images import ImageRepo
from PIL import Image
import io


class TestDeleteImages(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()
        img = Image.new('RGB', (1, 1))
        img_byte_arr = io.BytesIO()
        img.save(img_byte_arr, format='PNG')
        self.dummy_image = img_byte_arr.getvalue()

    def tearDown(self):
        self.ctx.pop()

    def test_delete_image(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.delete_one.return_value.deleted_count = 1
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.delete_image(id='111000000000000000000000')

        # Assert
        self.assertEqual(result, True)
        mock_collection.delete_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')}
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_delete_image_not_found(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.delete_one.return_value.deleted_count = 0
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.delete_image(id='111000000000000000000000')

        # Assert
        self.assertEqual(result, False)
        mock_collection.delete_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')}
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_delete_db_failure(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.delete_one.side_effect = Exception("Boom")
        self.app.config["db"].images = mock_collection
        image_repo = ImageRepo()

        # Act
        result = image_repo.delete_image(id='111000000000000000000000')

        # Assert
        self.assertIsInstance(result, DatabaseError)
        mock_collection.delete_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')}
        )
