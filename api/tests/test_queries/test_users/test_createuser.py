from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from models.pydantic import DatabaseError, UserOut, UserIn
from queries.user import User


class TestCreateUser(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_createuser(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.insert_one.return_value.inserted_id = ObjectId(
            '111000000000000000000000'
        )
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.createuser(
            UserIn(
                email="email@email.com",
                screen_name="name",
                password="hello",
            )
        )

        # Assert
        self.assertIsInstance(result, UserOut)
        self.assertEqual(result.id, '111000000000000000000000')
        self.assertEqual(result.email, "email@email.com")
        self.assertEqual(result.screen_name, "name")
        self.assertEqual(result.role, ["user"])
        self.assertEqual(result.chats, [])

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_createuser_database_error(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.insert_one.side_effect = Exception("Boom")
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.createuser(
            UserIn(
                email="email@email.com",
                screen_name="name",
                password="hello",
                role=["user"],
                chats=[]
            )
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)
        self.assertEqual(result.failure, "Could not create user")

        # Clean Up
        self.app.config["db"] = MagicMock()
