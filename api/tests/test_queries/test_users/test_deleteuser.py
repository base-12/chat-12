from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from models.pydantic import DatabaseError
from queries.user import User


class TestDeleteUser(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_deleteuser(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.delete_one.return_value.deleted_count = 1
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.deleteuser('111000000000000000000000')

        # Assert
        self.assertEqual(result, True)
        mock_collection.delete_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')}
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_deleteuser_not_found(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.delete_one.return_value.deleted_count = 0
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.deleteuser('111000000000000000000000')

        # Assert
        self.assertEqual(result, False)
        mock_collection.delete_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')}
        )

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_deleteuser_error(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.delete_one.side_effect = Exception("Boom")
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.deleteuser('111000000000000000000000')

        # Assert
        self.assertIsInstance(result, DatabaseError)
        self.assertEqual(result.failure, "Failed to delete user")

        # Clean Up
        self.app.config["db"] = MagicMock()
