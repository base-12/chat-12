from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from models.pydantic import DatabaseError, UserOutWithPassword
from queries.user import User


class TestGetUserWithPw(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_getuser_with_pw(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find_one.return_value = {
            "_id": ObjectId('111000000000000000000000'),
            "password": "password",
            "email": "email@email.com",
            "screen_name": "name",
            "role": ["user"],
            "chats": []
        }
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.getuser_with_pw(
            id="111000000000000000000000",
        )

        # Assert
        self.assertIsInstance(result, UserOutWithPassword)
        self.assertEqual(result.id, "111000000000000000000000")

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_getuser_with_pw_not_found(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find_one.return_value = None
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.getuser_with_pw(
            id="111000000000000000000000",
        )

        # Assert
        self.assertEqual(result, None)

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_getuser_with_pw_invalid_key(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find_one.return_value = [
            {
                "_id": ObjectId('111000000000000000000000'),
                "email": "email@email.com",
                "password": "password",
                "screen_name": "name",
                "role": ["user"],
                "chats": []
            }
        ]
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.getuser_with_pw(
            id="111000000000000000000000",
            boom="boom"
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)
        self.assertEqual(result.detail, "Invalid key boom")

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_getuser_without_pw_no_keys(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find_one.return_value = [
            {
                "_id": ObjectId('111000000000000000000000'),
                "password": "password",
                "email": "email@email.com",
                "screen_name": "name",
                "role": ["user"],
                "chats": []
            }
        ]
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.getuser_with_pw()

        # Assert
        self.assertIsInstance(result, DatabaseError)
        self.assertEqual(result.detail, 'No key provided')

        # Clean Up
        self.app.config["db"] = MagicMock()
