from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from models.pydantic import DatabaseError, UserOut
from queries.user import User


class TestGetUser(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_getuser(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find.return_value = [
            {
                "_id": ObjectId('111000000000000000000000'),
                "password": "password",
                "email": "email@email.com",
                "screen_name": "name",
                "role": ["user"],
                "chats": []
            }
        ]
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.getuser(
            id="111000000000000000000000",
        )

        # Assert
        self.assertEqual(result, [
            UserOut(
                id="111000000000000000000000",
                email="email@email.com",
                screen_name="name",
                role=["user"],
                chats=[]
            )
        ])

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_getuser_not_found(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find.return_value = []
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.getuser(
            id="111000000000000000000000",
        )

        # Assert
        self.assertEqual(result, [])

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_getuser_invalid_key(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find.return_value = [
            {
                "_id": ObjectId('111000000000000000000000'),
                "password": "password",
                "email": "email@email.com",
                "screen_name": "name",
                "role": ["user"],
                "chats": []
            }
        ]
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.getuser(
            id="111000000000000000000000",
            boom="boom"
        )

        # Assert
        self.assertIsInstance(result, DatabaseError)
        self.assertEqual(result.detail, "Invalid key boom")

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_getuser_no_keys(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find.return_value = [
            {
                "_id": ObjectId('111000000000000000000000'),
                "password": "password",
                "email": "email@email.com",
                "screen_name": "name",
                "role": ["user"],
                "chats": []
            },
            {
                "_id": ObjectId('222000000000000000000000'),
                "password": "password",
                "email": "email2@email.com",
                "screen_name": "name2",
                "role": ["user"],
                "chats": []
            }
        ]
        self.app.config["db"].users = mock_collection
        user_repo = User()

        # Act
        result = user_repo.getuser()

        # Assert
        self.assertEqual(len(result), 2)
        self.assertIsInstance(result[0], UserOut)
        self.assertIsInstance(result[1], UserOut)
        self.assertEqual(result[0].id, '111000000000000000000000')
        self.assertNotEqual(result[0].id, result[1].id)

        # Clean Up
        self.app.config["db"] = MagicMock()
