from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock, patch
from flask import Flask
from models.pydantic import DatabaseError, UserOut
from queries.user import User


class TestUpdateUser(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_updateuser(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.update_one.return_value.modified_count = 1
        self.app.config["db"].users = mock_collection
        user_repo = User()
        mock_get_user = MagicMock()
        mock_get_user.return_value = [
            UserOut(
                id="111000000000000000000000",
                email="email@email.com",
                screen_name="name",
                role=[],
                chats=["user"]
            )
        ]

        # Act
        with patch.object(User, "getuser", new=mock_get_user):
            result = user_repo.updateuser(
                id="111000000000000000000000",
                email="email@email.com"
            )

        # Assert
        mock_collection.update_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')},
            {"$set": {
                "email": "email@email.com",
            }}
        )
        self.assertEqual(result, UserOut(
            id="111000000000000000000000",
            email="email@email.com",
            screen_name="name",
            role=[],
            chats=["user"]
        ))

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_updateuser_not_found(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.update_one.return_value.modified_count = 0
        self.app.config["db"].users = mock_collection
        user_repo = User()
        mock_get_user = MagicMock()

        # Act
        with patch.object(User, "getuser", new=mock_get_user):
            result = user_repo.updateuser(
                id="111000000000000000000000",
                email="email@email.com"
            )

        # Assert
        self.assertIsInstance(result, DatabaseError)
        mock_get_user.assert_not_called()

        # Clean Up
        self.app.config["db"] = MagicMock()

    def test_updateuser_error(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.update_one.side_effect = Exception("Boom")
        self.app.config["db"].users = mock_collection
        user_repo = User()
        mock_get_user = MagicMock()
        mock_get_user.return_value = [
            UserOut(
                id="111000000000000000000000",
                email="email@email.com",
                screen_name="name",
                role=[],
                chats=["user"]
            )
        ]

        # Act
        with patch.object(User, "getuser", new=mock_get_user):
            result = user_repo.updateuser(
                id="111000000000000000000000",
                email="email@email.com"
            )

        # Assert
        mock_collection.update_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')},
            {"$set": {
                "email": "email@email.com",
            }}
        )
        self.assertIsInstance(result, DatabaseError)
        self.assertEqual(result.detail, "Boom")

        # Clean Up
        self.app.config["db"] = MagicMock()
