from datetime import datetime
from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from queries.chats import ChatRepo
from models.pydantic import ChatIn, ChatOut


class TestInsertChats(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_insert_chat(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.insert_one.return_value.inserted_id = ObjectId(
            '111000000000000000000000'
        )
        self.app.config["db"].chats = mock_collection
        chat_repo = ChatRepo()

        # Act
        result = chat_repo.create_chat(
            ChatIn(
                root_message_id="",
                tail_message_id="",
                position=(0, 0),
                created=datetime(2022, 1, 1, 0, 0)
            )
        )

        # Assert
        self.assertIsInstance(result, ChatOut)

        # Clean Up
        self.app.config["db"] = MagicMock()
