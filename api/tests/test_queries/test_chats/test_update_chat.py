from datetime import datetime
from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock, patch
from flask import Flask
from queries.chats import ChatRepo
from models.pydantic import ChatOut


def update_one(query: dict, update: dict):
    result = MagicMock()
    result.modified_count = 1 if (
        query.get("_id") == ObjectId('111000000000000000000000')
    ) else 0
    return result


class TestUpdateChats(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_update_chats(self):
        # Arrange
        fake_chat = ChatOut(
            id="111000000000000000000000",
            root_message_id="111000000000000000000001",
            tail_message_id="111000000000000000000002",
            position=(0, 0),
            created=datetime(2022, 1, 1, 0, 0, 0)
        )
        mock_collection = MagicMock()
        mock_collection.update_one = update_one
        self.app.config["db"].chats = mock_collection
        chat_repo = ChatRepo()
        mock_get_chats = MagicMock()
        mock_get_chats.return_value = [
            fake_chat
        ]

        # Act
        with patch.object(ChatRepo, "get_chat", new=mock_get_chats):
            result = chat_repo.update_chat(
                '111000000000000000000000',
                position=(10, 10)
            )

        # Assert
        self.assertEqual(result, fake_chat)
        mock_get_chats.assert_called_once_with(
            id='111000000000000000000000'
        )

        # Clean Up
        self.app.config["db"] = MagicMock()
