from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from queries.chats import ChatRepo


class TestDeleteChats(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_delete_chat(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.delete_one.return_value.deleted_count = 1
        self.app.config["db"].chats = mock_collection
        chat_repo = ChatRepo()

        # Act
        result = chat_repo.delete_chat(id='111000000000000000000000')

        # Assert
        self.assertEqual(result, True)
        mock_collection.delete_one.assert_called_once_with(
            {"_id": ObjectId('111000000000000000000000')}
        )

        # Clean Up
        self.app.config["db"] = MagicMock()
