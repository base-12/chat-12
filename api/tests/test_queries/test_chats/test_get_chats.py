from datetime import datetime
from unittest import TestCase
from bson import ObjectId
from unittest.mock import MagicMock
from flask import Flask
from queries.chats import ChatRepo
from models.pydantic import ChatOut


class TestGetChats(TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.config["db"] = MagicMock()  # Mock the db config value
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

    def test_get_images(self):
        # Arrange
        mock_collection = MagicMock()
        mock_collection.find.return_value = [
            {
                "_id": ObjectId('111000000000000000000000'),
                "root_message_id": "111000000000000000000001",
                "tail_message_id": "111000000000000000000002",
                "position": (0, 0),
                "created": datetime(2022, 1, 1, 0, 0)
            }
        ]
        self.app.config["db"].chats = mock_collection
        chat_repo = ChatRepo()

        # Act
        result = chat_repo.get_chat(id='111000000000000000000000')
        # Assert
        self.assertEqual(result, [
            ChatOut(
                id='111000000000000000000000',
                root_message_id="111000000000000000000001",
                tail_message_id="111000000000000000000002",
                position=(0, 0),
                created=datetime(2022, 1, 1, 0, 0)
            )
        ])

        # Clean Up
        self.app.config["db"] = MagicMock()
