from unittest import TestCase
from unittest.mock import patch
from flask import Flask
from models.pydantic import DatabaseError, UserOut
from routes.users import init_routes


def fake_crypt(a, b):
    return a == b


class TestUpdateUserEndpoint(TestCase):
    def setUp(self) -> None:
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.secret_key = "SECRET_KEY"
        init_routes(self.app)

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_update_user_success(self, mock_repo, mock_crypt):
        # Arrange
        user_out = UserOut(
            id="111000000000000000000000",
            email="email2",
            screen_name="name",
            role=["user"],
            chats=[]
        )
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "example_password"
        mock_updateuser = mock_repo_instance.updateuser
        mock_updateuser.return_value = user_out
        expected_result = user_out.model_dump()

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {"id": "1", "role": ["admin", "user"]}

            response = client.put(
                "/users",
                json={
                    "password": "example_password",
                    "email": "email2"
                }
            )

            # Assert
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json, expected_result)
            mock_updateuser.assert_called_once_with(id="1", email="email2")

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_update_user_admin_success(self, mock_repo, mock_crypt):
        # Arrange
        user_out = UserOut(
            id="111000000000000000000000",
            email="email2",
            screen_name="name",
            role=["user"],
            chats=[]
        )
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "example_password"
        mock_updateuser = mock_repo_instance.updateuser
        mock_updateuser.return_value = user_out
        expected_result = user_out.model_dump()

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {
                    "id": "111000000000000000000000",
                    "role": ["user", "admin"]
                }

            response = client.put(
                "/users",
                json={
                    "id": "222000000000000000000000",
                    "password": "example_password",
                    "email": "email2"
                }
            )

            # Assert
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content_type, "application/json")
            self.assertEqual(response.json, expected_result)
            mock_updateuser.assert_called_once_with(
                id="222000000000000000000000",
                email="email2"
            )

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_update_user_admin_updateself(self, mock_repo, mock_crypt):
        # Arrange
        user_out = UserOut(
            id="111000000000000000000000",
            email="email2",
            screen_name="name",
            role=["user"],
            chats=[]
        )
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "example_password"
        mock_updateuser = mock_repo_instance.updateuser
        mock_updateuser.return_value = user_out
        expected_result = user_out.model_dump()

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {
                    "id": "111000000000000000000000",
                    "role": ["user"]
                }

            response = client.put(
                "/users",
                json={
                    "id": "111000000000000000000000",
                    "password": "example_password",
                    "email": "email2"
                }
            )

            # Assert
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content_type, "application/json")
            self.assertEqual(response.json, expected_result)
            mock_updateuser.assert_called_once_with(
                id="111000000000000000000000",
                email="email2"
            )

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_update_user_bad_password(self, mock_repo, mock_crypt):
        # Arrange
        user_out = UserOut(
            id="111000000000000000000000",
            email="email2",
            screen_name="name",
            role=["user"],
            chats=[]
        )
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "password"
        mock_updateuser = mock_repo_instance.updateuser
        mock_updateuser.return_value = user_out

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {
                    "id": "111000000000000000000000",
                    "role": ["admin", "user"]
                }

            response = client.put(
                "/users",
                json={
                    "password": "example_password",
                    "email": "email2"
                }
            )

            # Assert
            self.assertEqual(response.status_code, 403)
            self.assertEqual(response.content_type, "application/json")
            self.assertEqual(response.json.get("Error"), "Invalid Password")
            mock_updateuser.assert_not_called()

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_update_user_unauthorized(self, mock_repo, mock_crypt):
        # Arrange
        user_out = UserOut(
            id="111000000000000000000000",
            email="email2",
            screen_name="name",
            role=["user"],
            chats=[]
        )
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "example_password"
        mock_updateuser = mock_repo_instance.updateuser
        mock_updateuser.return_value = user_out

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {
                    "id": "111000000000000000000000",
                    "role": ["user"]
                }

            response = client.put(
                "/users",
                json={
                    "id": "222000000000000000000000",
                    "password": "example_password",
                    "email": "email2"
                }
            )

            # Assert
            self.assertEqual(response.status_code, 401)
            self.assertEqual(response.content_type, "application/json")
            self.assertEqual(response.json.get("Error"), "User not authorized")
            mock_updateuser.assert_not_called()

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_update_user_db_error(self, mock_repo, mock_crypt):
        # Arrange
        user_out = DatabaseError(
            detail="Boom",
            failure="Failed to update user"
        )
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "example_password"
        mock_updateuser = mock_repo_instance.updateuser
        mock_updateuser.return_value = user_out

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {
                    "id": "111000000000000000000000",
                    "role": ["user", "admin"]
                }

            response = client.put(
                "/users",
                json={
                    "id": "222000000000000000000000",
                    "password": "example_password",
                    "email": "email2"
                }
            )

            # Assert
            self.assertEqual(response.status_code, 500)
            self.assertEqual(response.content_type, "application/json")
            self.assertEqual(
                response.json.get("Error"),
                "Failed to update user"
            )
            mock_updateuser.assert_called_once_with(
                id="222000000000000000000000",
                email="email2"
            )
