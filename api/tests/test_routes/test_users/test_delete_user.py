from unittest import TestCase
from unittest.mock import patch
from flask import Flask
from routes.users import init_routes


def fake_crypt(a, b):
    return a == b


class TestDeleteUserEndpoint(TestCase):
    def setUp(self) -> None:
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        self.app.secret_key = "SECRET_KEY"
        init_routes(self.app)

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_delete_user_success(self, mock_repo, mock_crypt):
        # Arrange
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "example_password"
        mock_deleteuser = mock_repo_instance.deleteuser
        mock_deleteuser.return_value = True

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {"id": "1", "role": ["admin", "user"]}

            response = client.delete(
                "/users", json={
                    "password": "example_password",
                    "id": "1"
                })

            # Assert
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json, True)
            mock_deleteuser.assert_called_once_with("1")

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_delete_user_unauthorized(self, mock_repo, mock_crypt):
        # Arrange
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "example_password"
        mock_deleteuser = mock_repo_instance.deleteuser
        mock_deleteuser.return_value = True

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {"id": "1", "role": ["user"]}

            response = client.delete(
                "/users", json={
                    "password": "example_password",
                    "id": "1"
                })

            # Assert
            self.assertEqual(response.status_code, 401)
            self.assertEqual(response.json.get("Error"), "User not authorized")
            mock_deleteuser.assert_not_called()

    @patch("routes.users.pbkdf2_sha256")
    @patch("routes.users.User")
    def test_delete_user_bad_password(self, mock_repo, mock_crypt):
        # Arrange
        mock_crypt.verify = fake_crypt
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser_with_pw
        mock_getuser.return_value.password = "password"
        mock_deleteuser = mock_repo_instance.deleteuser
        mock_deleteuser.return_value = True

        # Act
        with self.app.test_client() as client:
            with client.session_transaction() as session:
                session["logged_in"] = True
                session["user"] = {"id": "1", "role": ["user", "admin"]}

            response = client.delete(
                "/users", json={
                    "id": "1",
                    "password": "example_password"
                })

            # Assert
            self.assertEqual(response.status_code, 403)
            self.assertEqual(response.json.get("Error"), "Invalid Password")
            mock_deleteuser.assert_not_called()
