from unittest import TestCase
from unittest.mock import patch
from flask import Flask
from models.pydantic import DatabaseError, UserOut
from routes.users import init_routes


class TestGetUserEndpoint(TestCase):
    def setUp(self) -> None:
        self.app = Flask(__name__)
        self.app.config["TESTING"] = True
        init_routes(self.app)
        self.client = self.app.test_client()

    @patch("routes.users.User")
    def test_get_users(self, mock_repo):
        # Arrange
        users = [
            UserOut(
                id='111000000000000000000000',
                email='mail',
                screen_name="name",
                role=["user"],
                chats=[]
            ),
            UserOut(
                id='222000000000000000000000',
                email='mail2',
                screen_name="name2",
                role=["user"],
                chats=[]
            )
        ]
        mock_repo.return_value.getuser.return_value = users

        # Act
        request = self.client.get("/users", json={})
        data = request.json

        # Assert
        self.assertEqual(request.status, "200 OK")
        self.assertIsInstance(data, list)
        self.assertEqual(len(data), 2)
        self.assertIsInstance(data[0], dict)
        self.assertIsInstance(data[1], dict)
        self.assertEqual(data[0].get("email"), "mail")
        self.assertEqual(data[1].get("email"), "mail2")

        # Clean up

    @patch("routes.users.User")
    def test_getuser_by_role(self, mock_repo):
        # Arrange
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser
        mock_getuser.return_value = []

        # Act
        result = self.client.get("/users", json={
            "role": ["user", "admin"]
        })

        # Assert
        self.assertEqual(result.status, "200 OK")
        self.assertEqual(result.content_type, "application/json")
        mock_repo.assert_called_once()
        mock_getuser.assert_called_once()
        mock_getuser.assert_called_once_with(role=["user", "admin"])
        self.assertEqual(result.content_type, "application/json")

        # Clean Up

    @patch("routes.users.User")
    def test_getuser_bad_key(self, mock_repo):
        # Arrange
        mock_repo_instance = mock_repo.return_value
        mock_getuser = mock_repo_instance.getuser
        mock_getuser.return_value = DatabaseError(
            detail="Invalid key bad_key",
            failure="Failed to get user"
        )

        # Act
        result = self.client.get("/users", json={
            "bad_key": "None"
        })

        # Assert
        self.assertEqual(result.status_code, 500)
        mock_repo.assert_called_once()
        mock_getuser.assert_called_once()
        mock_getuser.assert_called_once_with(bad_key="None")
        self.assertEqual(result.content_type, "application/json")
        self.assertEqual(result.json.get("Error"), "Invalid key bad_key")

        # Clean Up
