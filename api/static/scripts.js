$("form[name=signup]").submit(function(e) {
  e.preventDefault();
  var $form = $(this);
  var $error = $form.find(".error");
  var data = $form.serialize();

  $.ajax({
    type: "POST",
    url: "/signup/backend",
    data: data,
    dataType: "json",
    success: function(resp) {
      window.location.href = "/"
    },
    error: function(resp) {
      $error.text(resp.responseJSON.error).removeClass("error--hidden");
    }
  });
  e.preventDefault();
});


$("form[name=login]").submit(function(e) {
  e.preventDefault();
  var $form = $(this);
  var $error = $form.find(".error");
  var data = $form.serialize();

  $.ajax({
    type: "POST",
    url: "/login/backend",
    data: data,
    dataType: "json",
    success: function(resp) {
      window.location.href = "/"
    },
    error: function(resp) {
      $error.text(resp.responseJSON.error).removeClass("error--hidden");
    }
  });
  e.preventDefault();
});
