from datetime import datetime
from pydantic import BaseModel
from typing import List, Tuple


class UserIn(BaseModel):
    email: str
    screen_name: str
    password: str
    role: List[str] = ["user"]
    chats: List[str] = []


class UserOut(BaseModel):
    id: str
    email: str
    screen_name: str
    role: List[str] = []
    chats: List[str] = []


class UserOutWithPassword(UserOut):
    password: str


class ImageIn(BaseModel):
    alt: str
    content: bytes


class ImageOut(BaseModel):
    id: str
    alt: str
    content: bytes


class ChatIn(BaseModel):
    root_message_id: str
    tail_message_id: str
    position: Tuple[int, int]
    created: datetime


class ChatOut(BaseModel):
    id: str
    root_message_id: str
    tail_message_id: str
    position: Tuple[int, int]
    created: datetime


class MessageIn(BaseModel):
    title: str
    content: str
    parent_message_id: str | None = None
    child_message_id: str | None = None
    rich_text: bool
    created: datetime


class MessageOut(BaseModel):
    id: str
    title: str
    content: str
    parent_message_id: str | None
    child_message_id: str | None
    rich_text: bool
    created: datetime


class DatabaseError(BaseModel):
    detail: str
    failure: str
