from flask import Flask
from routes.routes import init_routes
from routes.users import init_routes as init_users_routes
from queries.pymongo import init_db
from flask_cors import CORS
import os

CORS_HOST = os.environ.get("CORS_HOST")
if not CORS_HOST:
    raise ValueError("CORS_HOST environment variable is not set")

SECRET_KEY = os.environ.get("SECRET_KEY")
if not SECRET_KEY:
    raise ValueError("SECRET_KEY environment variable is not set")


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = SECRET_KEY
    CORS(app, origins="CORS_HOST")
    init_routes(app)
    init_users_routes(app)
    app.config["db"] = init_db(app)
    return app


app = create_app()
