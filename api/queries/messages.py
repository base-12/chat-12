from typing import List
from bson import ObjectId
from models.pydantic import MessageIn, MessageOut, DatabaseError
from flask import current_app


class MessageRepo:
    def __init__(self):
        self.db = current_app.config["db"]

    def insert_message(self, message: MessageIn) -> MessageOut | DatabaseError:
        """
        Insert a `Message` into the database.

        Takes a MessageIn object as an argument. If there is a
        `parent_message_id` on the MessageIn object, the `child_message_id` of
        the parent will be updated to the id of the newly inserted message.

        IMPORTANT: The `parent_message_id` and `child_message_id` fields are
        not validated. If a message has a `parent_message_id` that does not
        exist, or a `child_message_id` that does exist, the database will be
        left in an inconsistent state.

        Returns a MessageOut object if successful, or a DatabaseError object
        if not.
        """
        try:
            collection = self.db.messages
            parent = None
            child = None
            if message.parent_message_id:
                parent = collection.find_one(
                    {"_id": ObjectId(message.parent_message_id)}
                )
                if not parent or parent.get("child_message_id"):
                    return DatabaseError(
                        detail=""
                        "Parent does not exist" if not parent else ""
                        "Parent already has a child",
                        failure="Failed to insert message"
                    )
            if message.child_message_id:
                child = collection.find_one(
                    {"child_message_id": message.child_message_id}
                )
                if child:
                    return DatabaseError(
                        detail="Message with that child id already exists",
                        failure="Failed to insert message"
                    )
            result = collection.insert_one(message.model_dump())
            if parent:
                self.update_message(
                    id=str(parent.get("_id")),
                    child_message_id=str(result.inserted_id)
                )
            if message.child_message_id:
                self.update_message(
                    id=message.child_message_id,
                    parent_message_id=str(result.inserted_id)
                )
            out = MessageOut(
                id=str(result.inserted_id),
                **message.model_dump(),
            )
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to insert message"
            )

    def get_messages(
            self,
            **kwargs
    ) -> List[MessageOut] | DatabaseError:
        """
        Get messages from the database.

        Takes keyword arguments to filter the messages. If an `id` keyword is
        passed, it will be converted to an `_id` keyword and the value will be
        converted to an ObjectId. If any other keyword is passed that is not a
        field on the MessageIn model, a DatabaseError will be returned.

        Returns a list of MessageOut objects if successful, or a DatabaseError
        if not.
        """
        if kwargs.get("id"):
            kwargs["_id"] = ObjectId(kwargs.pop("id"))
        for key in kwargs.keys():
            if not (key == "_id" or key in MessageIn.model_fields.keys()):
                return DatabaseError(
                    detail=f"Invalid key {key}",
                    failure="Failed to get message"
                )
        try:
            collection = self.db.messages
            documents = collection.find(kwargs)
            out = []
            for r in documents:
                out.append(db_to_MessageOut(r))
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure=f"Failed to get messages by {kwargs}"
            )

    def update_message(
            self,
            id: str,
            **kwargs
    ) -> MessageOut | DatabaseError:
        """
        Update a message in the database.

        Takes an id and a dictionary object as arguments. The relevant fields
        on the matching message will be updated with the values in the dict.

        IMPORTANT: Changing the `parent_message_id` or `child_message_id`
        fields will NOT update the corresponding fields on the parent or child
        messages.

        Returns a MessageOut object if successful, or a DatabaseError object if
        not.
        """
        for key in kwargs.keys():
            if key not in MessageIn.model_fields.keys():
                return DatabaseError(
                    detail=f"Invalid key {key}",
                    failure="Failed to update message"
                )
        try:
            collection = self.db.messages
            result = collection.update_one(
                {"_id": ObjectId(id)},
                {"$set": kwargs}
            )

            if result.modified_count == 1:
                return self.get_messages(id=id)[0]
            else:
                return DatabaseError(
                    detail="No message was found with that id",
                    failure="Failed to update message"
                )
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to update message"
            )

    def delete_message(self, id: str) -> bool | DatabaseError:
        """
        Delete a message from the database.

        Takes an id as an argument. If the message has a `parent_message_id` or
        `child_message_id` field, the corresponding field on the parent or
        child message will be updated to maintain the link.

        Returns a boolean indicating if a record was deleted if successful, or
        a DatabaseError object if not.
        """
        try:
            collection = self.db.messages
            document = collection.find_one({"_id": ObjectId(id)})
            if document is None:
                return False
            parent = document.get("parent_message_id")
            child = document.get("child_message_id")
            if parent:
                self.update_message(
                    document.get("parent_message_id"),
                    child_message_id=document.get("child_message_id")
                )
            if child:
                self.update_message(
                    document.get("child_message_id"),
                    parent_message_id=document.get("parent_message_id")
                )
            result = collection.delete_one({"_id": ObjectId(id)})

            return result.deleted_count == 1
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to delete message"
            )

    def get_message_thread(
            self,
            id: str
    ) -> List[MessageOut] | DatabaseError:
        """
        Get a list of messages in a thread.

        Takes the id of the root message as an argument. The thread will be
        built by following the `child_message_id` field of each message.

        Returns a list of MessageOut objects if successful, or a DatabaseError
        object if not.
        """
        try:
            collection = self.db.messages
            document = collection.find_one({"_id": ObjectId(id)})
            out = []
            while document:
                out.append(db_to_MessageOut(document))
                document = collection.find_one(
                    {"_id": ObjectId(document.get("child_message_id"))}
                )
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to get message thread"
            )

    def get_recent_messages(
            self,
            tail: str,
            count: int = 50,
    ) -> List[MessageOut] | DatabaseError:
        """
        Get a list of recent messages.

        Takes the id of the tail message as an argument. The thread will be
        built by following the `parent_message_id` field of each message.

        Returns a list of MessageOut objects if successful, or a DatabaseError
        object if not.
        """
        try:
            collection = self.db.messages
            document = collection.find_one({"_id": ObjectId(tail)})
            out = []
            while document and count > 0:
                out.append(db_to_MessageOut(document))
                document = collection.find_one(
                    {"_id": ObjectId(document.get("parent_message_id"))}
                )
                count -= 1
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to get recent messages"
            )


def db_to_MessageOut(document: dict):
    return MessageOut(
        id=str(document.get("_id")),
        **document
    )
