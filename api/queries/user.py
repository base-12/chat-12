from typing import List
from flask import current_app
from passlib.hash import pbkdf2_sha256
from models.pydantic import DatabaseError, UserOut, UserIn, UserOutWithPassword
from bson import ObjectId


class User:

    def __init__(self) -> None:
        self.db = current_app.config["db"]

    def createuser(self, user: UserIn):
        try:
            user.password = pbkdf2_sha256.hash(user.password)
            result = self.db.users.insert_one(user.model_dump())
            return UserOut(id=str(result.inserted_id), **user.model_dump())
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Could not create user"
            )

    def getuser(self, **kwargs) -> List[UserOut] | DatabaseError:
        if kwargs.get("id"):
            kwargs["_id"] = ObjectId(kwargs.pop("id"))
        for key in kwargs.keys():
            if not (key == "_id" or key in UserIn.model_fields.keys()):
                return DatabaseError(
                    detail=f"Invalid key {key}",
                    failure="Failed to get user"
                )
            if key == "role":
                if not isinstance(kwargs["role"], list):
                    kwargs["role"] = [kwargs["role"]]
                kwargs["role"] = {"$in": kwargs["role"]}
            if key == "chats":
                if not isinstance(kwargs["chats"], list):
                    kwargs["chats"] = [kwargs["chats"]]
                kwargs["chats"] = {"$in": kwargs["chats"]}

        try:
            collection = self.db.users
            if kwargs == {}:
                documents = collection.find()
            else:
                documents = collection.find(kwargs)
            out = []
            for r in documents:
                out.append(UserOut(id=str(r["_id"]), **r))
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure=f"Failed to get Users by {kwargs}"
            )

    def getuser_with_pw(
        self,
        **kwargs
    ) -> UserOutWithPassword | DatabaseError:
        if kwargs == {}:
            return DatabaseError(
                detail="No key provided",
                failure="Failed to get user with password"
            )
        if kwargs.get("id"):
            kwargs["_id"] = ObjectId(kwargs.pop("id"))
        for key in kwargs.keys():
            if key not in ["email", "screen_name", "_id"]:
                return DatabaseError(
                    detail=f"Invalid key {key}",
                    failure="Failed to get user with password"
                )
        try:
            collection = self.db.users
            user = collection.find_one(kwargs)
            if user:
                out = UserOutWithPassword(id=str(user["_id"]), **user)
                return out
            else:
                return None
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure=f"Failed to get user by {kwargs}"
            )

    def updateuser(self, id: str, **kwargs):
        for key in kwargs.keys():
            if key not in UserIn.model_fields.keys():
                return DatabaseError(
                    detail=f"Invalid key {key}",
                    failure="Failed to update user"
                )
        try:
            collection = self.db.users
            result = collection.update_one(
                {"_id": ObjectId(id)},
                {"$set": kwargs}
            )
            if result.modified_count == 1:
                return self.getuser(id=str(id))[0]
            else:
                return DatabaseError(
                    detail=str(result),
                    failure="Failed to update user"
                )
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to update user"
            )

    def deleteuser(self, id: str):
        try:
            collection = self.db.users
            result = collection.delete_one({"_id": ObjectId(id)})
            return result.deleted_count == 1
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to delete user"
            )
