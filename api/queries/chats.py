from models.pydantic import ChatIn, ChatOut, DatabaseError
from typing import List
from bson import ObjectId
from flask import current_app


class ChatRepo:
    def __init__(self):
        self.db = current_app.config["db"]

    def create_chat(self, chat: ChatIn) -> ChatOut | DatabaseError:
        try:
            collection = self.db.chats
            result = collection.insert_one(chat.model_dump())

            out = ChatOut(
                id=str(result.inserted_id),
                **chat.model_dump(),
            )
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to create chat"
            )

    def get_chat(self, **kwargs) -> List[ChatOut] | DatabaseError:
        if kwargs.get("id"):
            kwargs["_id"] = ObjectId(kwargs.pop("id"))
        for key in kwargs.keys():
            if key in ChatIn.model_fields.keys() and not key == "_id":
                return DatabaseError(
                    detail=f"Invalid Key {key}",
                    failure="Failed to get message"
                )
        try:
            collection = self.db.chats
            documents = collection.find(kwargs)
            out = []
            for record in documents:
                record = ChatOut(id=str(record.get("_id")), **record)
                out.append(record)
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to get chats with provided. "
                f"Provided: {kwargs}",
            )

    def delete_chat(self, id: str) -> ChatOut | DatabaseError:
        try:
            collection = self.db.chats
            result = collection.delete_one({"_id": ObjectId(id)})
            return result.deleted_count == 1
        except Exception as e:
            print(e)
            return DatabaseError(
                detail=str(e),
                failure="Failed to delete chat"
            )

    def update_chat(self, id: str, **kwargs) -> ChatOut | DatabaseError:
        for key in kwargs.keys():
            if key not in ChatIn.model_fields.keys():
                return DatabaseError(
                    detail=f"Invalid key {key}",
                    failure="failed to update chat, key not in keys"
                )
        try:
            collection = self.db.chats
            result = collection.update_one(
                {"_id": ObjectId(id)},
                {"$set": kwargs}
            )

            if result.modified_count == 1:
                return self.get_chat(id=id)[0]
            else:
                return DatabaseError(
                    detail="No chat was found with that id",
                    failure="Failed to update chat"
                )
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to update message"
            )

    def get_all(self):
        collection = self.db.chats
        out = []
        try:
            for record in collection:
                record = ChatOut(id=str(record.get("_id")), **record)
                out.append(record)
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to get chats",
            )


def db_to_out(document: dict):
    return ChatOut(
        id=str(document.get("_id")),
        **document
    )
