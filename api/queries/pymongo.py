from flask_pymongo import PyMongo
import os

MONGO_URI = os.environ.get("MONGO_URI")
if not MONGO_URI:
    raise ValueError("MONGO_URI environment variable is not set")


def init_db(app):
    pymongo = PyMongo(
        app,
        uri=MONGO_URI
    )
    return pymongo.db
