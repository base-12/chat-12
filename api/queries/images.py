from typing import List
from bson import ObjectId
from models.pydantic import ImageIn, ImageOut, DatabaseError
from flask import current_app


class ImageRepo:
    def __init__(self):
        self.db = current_app.config["db"]

    def insert_image(self, image: ImageIn) -> ImageOut | DatabaseError:
        """
        Insert an `Image` into the database.

        Takes an ImageIn object as an argument.

        Returns an ImageOut object if successful, or a DatabaseError object if
        not.
        """
        try:
            collection = self.db.images
            result = collection.insert_one(image.model_dump())
            if result.inserted_id:
                return ImageOut(
                    id=str(result.inserted_id),
                    **image.model_dump()
                )
            else:
                return DatabaseError(
                    detail="Failed to insert image",
                    failure="Failed to insert image"
                )
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to insert image"
            )

    def get_images(self, **kwargs) -> List[ImageOut] | DatabaseError:
        """
        Get an `Image` from the database.

        Takes keyword arguments to filter the image. If an `id` keyword is
        provided, it will be converted to an ObjectId. If any other keyword is
        provided that is not a field in the Image model, a DatabaseError will
        be returned.

        Returns an list of ImageOut objects.
        """
        if kwargs.get("id"):
            kwargs["_id"] = ObjectId(kwargs.pop("id"))
        for key in kwargs:
            if not (key == "_id" or key in ImageIn.model_fields.keys()):
                return DatabaseError(
                    detail=f"Invalid key {key}",
                    failure="Failed to get image"
                )
        try:
            collection = self.db.images
            documents = collection.find(kwargs)
            out = []
            for r in documents:
                out.append(ImageOut(id=str(r.get("_id")), **r))
            return out
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to get image"
            )

    def update_image(self, id: str, **kwargs) -> ImageOut | DatabaseError:
        """
        Update an `Image` in the database.

        Takes an `id` string as an argument and any other keyword arguments to
        update the image. If any other keyword is provided that is not a field
        in the Image model, a DatabaseError will be returned.

        Returns an ImageOut object if successful, or a DatabaseError object if
        not.
        """
        try:
            collection = self.db.images
            if kwargs.get("id"):
                kwargs["_id"] = ObjectId(kwargs.pop("id"))
            for key in kwargs:
                if key not in ImageIn.model_fields.keys():
                    return DatabaseError(
                        detail=f"Invalid key {key}",
                        failure="Failed to update image"
                    )
            result = collection.update_one(
                {"_id": ObjectId(id)},
                {"$set": kwargs}
            )
            if result.modified_count:
                return self.get_images(id=id)[0]
            return DatabaseError(
                detail="Image not found",
                failure="Failed to update image"
            )
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to update image"
            )

    def delete_image(self, id: str) -> bool | DatabaseError:
        """
        Delete an `Image` from the database.

        Takes an `id` string as an argument.

        Returns a boolean indicating if a record was deleted if successful, or
        a DatabaseError object if not.
        """
        try:
            collection = self.db.images
            result = collection.delete_one({"_id": ObjectId(id)})
            return result.deleted_count == 1
        except Exception as e:
            return DatabaseError(
                detail=str(e),
                failure="Failed to delete image"
            )
