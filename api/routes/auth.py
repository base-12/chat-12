from models.pydantic import UserIn
from flask import jsonify, session, redirect
from functools import wraps


def login_required(f):
    @wraps(f)
    def wrap(*arg, **kwargs):
        if "logged_in" in session:
            return f(*arg, **kwargs)
        else:
            return redirect("/")

    return wrap


def start_session(user: UserIn):
    user = user.model_dump()
    if user.get("password"):
        del user["password"]
    session["logged_in"] = True
    session["user"] = user
    return jsonify(user), 200


def signout():
    session.clear()
    return redirect("/")
