from datetime import datetime
from models.pydantic import MessageIn, UserIn
from queries.user import User
from queries.messages import MessageRepo
from flask import render_template, jsonify, request
from routes.auth import start_session, signout, login_required
from passlib.hash import pbkdf2_sha256
from bson import json_util


def model_to_json(data):
    if isinstance(data, (bool, str, int, float)):
        return jsonify(data)
    out = None
    try:
        len(data)
        out = []
        for model in data:
            out.append(model.model_dump())
    except TypeError or AttributeError:
        if hasattr(data, "model_dump"):
            out = data.model_dump()
        else:
            out = data
    return jsonify(out)


def init_routes(app):
    @app.route("/signup/backend", methods=["POST", "GET"])
    def signup():
        user = UserIn(
            email=request.form.get("email"),
            screen_name=request.form.get("screen_name"),
            password=request.form.get("password"),
        )
        user_out = User().createuser(user)
        return start_session(user_out)

    @app.route("/", methods=["GET", "POST"])
    def home():
        return render_template("index.html")

    @app.route("/signup", methods=["POST", "GET"])
    def sign():
        return render_template("SignUp.html")

    @app.route("/login/backend", methods=["POST", "GET"])
    def login():
        if request.method == "POST":
            user = User().getuser_with_pw(email=request.form.get("email"))
            verified = pbkdf2_sha256.verify(
                request.form.get("password"), user.password
            )
            if verified:
                return start_session(user)
            else:
                return jsonify({"Error": "Invalid Password"})
        else:
            return render_template("LogIn.html")

    @app.route("/signout")
    @login_required
    def logout():
        return signout()

    @app.route("/messages/create", methods=["POST"])
    def create_message():
        if request.method == "POST":
            create_message = MessageIn(**request.form, created=datetime.now())
            message = MessageRepo().insert_message(create_message)
            return json_util.dumps(message.model_dump())

    @app.route("/messages", methods=["GET"])
    def get_all_messages():
        message_out = []
        message_in = MessageRepo().get_messages()
        for message in message_in:
            message_out.append(message.model_dump())
        return json_util.dumps(message_out)

    @app.route("/messages/<id>", methods=["PUT"])
    def update_message(id):
        if request.method == "PUT":
            updated_message = MessageRepo().update_message(id, **request.form)
            return json_util.dumps(updated_message.model_dump())

    @app.route("/messages/<id>", methods=["DELETE"])
    def delete_message(id):
        if request.method == "DELETE":
            delete_message = MessageRepo().delete_message(id)
            if isinstance(delete_message, bool):
                return json_util.dumps(delete_message)
            else:
                return json_util.dumps(delete_message.model_dump())

    @app.route("/messages/recent/<id>", methods=["GET"])
    def get_recent_message(id):
        if request.method == "GET":
            message_out = []
            recent_message = MessageRepo().get_recent_messages(id)
            for message in recent_message:
                message_out.append(message.model_dump())
            return json_util.dumps(message_out)

    @app.route("/messages/thread/<id>", methods=["GET"])
    def get_message_thread(id):
        if request.method == "GET":
            message_out = []
            message_thread = MessageRepo().get_message_thread(id)
            for message in message_thread:
                message_out.append(message.model_dump())
            return json_util.dumps(message_out)
