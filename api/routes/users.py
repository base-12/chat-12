from models.pydantic import DatabaseError
from queries.user import User
from passlib.hash import pbkdf2_sha256
from routes.routes import model_to_json, login_required
from flask import session, request, jsonify


def init_routes(app):
    @app.route("/users", methods=["GET"])
    def get_user():
        '''
        Optionally, takes a json body containing User data keys
        '''
        repo = User()
        users = []
        if request.content_type == "application/json":
            args = request.get_json()
            users = repo.getuser(**args)
        else:
            users = repo.getuser()
        if isinstance(users, DatabaseError):
            return jsonify({"Error": users.detail}), 500
        return model_to_json(users)

    @login_required
    @app.route("/users", methods=["PUT"])
    def update_user():
        '''
        Takes a json object with a [optional] user id, password, and the keys
        to be updated
        '''
        repo = User()
        update_id = session["user"]["id"]
        if request.json.get('id'):
            update_id = request.json.pop("id")
        user = repo.getuser_with_pw(
            id=session["user"]["id"]
        )
        verified = pbkdf2_sha256.verify(
            request.json.pop("password"),
            user.password,
        )
        if not verified:
            return jsonify({"Error": "Invalid Password"}), 403

        if not update_id == session["user"]["id"]:
            if "admin" not in session["user"]["role"]:
                return jsonify({"Error": "User not authorized"}), 401
        result = repo.updateuser(
            id=update_id,
            **request.json
        )
        if isinstance(result, DatabaseError):
            return jsonify({"Error": result.failure}), 500
        return model_to_json(result)

    @login_required
    @app.route("/users", methods=["DELETE"])
    def delete_user():
        '''
        Takes a json body containing the user's password the user id to delete

        If the password is correct and the logged in user is an admin,
        deletes the account of the given user id.
        '''
        repo = User()
        user = repo.getuser_with_pw(
            id=session["user"]["id"]
        )
        verified = pbkdf2_sha256.verify(
            request.json.get("password"),
            user.password
        )
        if not request.json.get("id"):
            return jsonify({"Error": "Bad Request"}), 400
        if not verified:
            return jsonify({"Error": "Invalid Password"}), 403
        if "admin" not in session["user"]["role"]:
            return jsonify({"Error": "User not authorized"}), 401

        result = repo.deleteuser(request.json.get("id"))
        if isinstance(result, DatabaseError):
            return jsonify({"Error": result.failure}), 500
        return model_to_json(result)
